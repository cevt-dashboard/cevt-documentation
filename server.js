const express = require("express"),
  app = express(),
  http = require("http").Server(app);
const csv = require("csv-parser");
const fs = require("fs");
const axios = require("axios");

app.get("/populate", async function (req, res, next) {
  let data = [];
  fs.createReadStream("enseignants_sample.csv")
    .pipe(csv())
    .on("data", (row) => {
      console.log(row);
      data.push(row);
    })
    .on("end", () => {
      data.forEach((x) => {
        axios
          .post(
            "https://localhost:9090/api/v1/teachers",

            {
              cm: 1,
              td: 1,
              tp: 1,
              userId: x.UserId,
              lastName: x.Nom,
              firstName: x.Prenom,
              email: x.Email,
              status: x.Statut,
              quotite: x.Quotite,
              department: x.Departement,
              composante: x.Composante,
              service: x.Service,
              pourcentage: x.pourcentage,
            }
          )
          .then((res) => {})
          .catch((error) => {
            console.error(error);
          });
      });
    });
});

var port = process.env.PORT || 4000;
http.listen(port, function () {
  console.log("server on!: http://localhost:4000/");
});

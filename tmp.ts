
[  {
    "name": "M2MIAA",
    "link": "https://edt.univ-evry.fr/icsetudiant/m2miaa.ics"
  },
    {
    "name": "M1MIAA",
    "link": "https://edt.univ-evry.fr/icsetudiant/m1miaa.ics"
  },
    {
    "name": "M2MIAI",
    "link": "https://edt.univ-evry.fr/icsetudiant/m2miai_etudiant(e).ics"
  },
    {
    "name": "M1MIAI",
    "link": "https://edt.univ-evry.fr/icsetudiant/m1miai_etudiant(e).ics"
  },

  {
    "name": "M2MSA",
    "link": "https://edt.univ-evry.fr/icsetudiant/m2sa_etudiant(e).ics"
  },
  {
    "name": "M2SR",
    "link": "https://edt.univ-evry.fr/icsetudiant/m2sr_etudiant(e).ics"
  },
 
  {
    "name": "L3MIAI",
    "link": "https://edt.univ-evry.fr/icsetudiant/l3miai_etudiant(e).ics"
  },
  {
    "name": "L3MIAA",
    "link": "https://edt.univ-evry.fr/icsetudiant/l3miaa.ics"
  }
  
]